package com.notarius.urlshortener.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static javax.servlet.http.HttpServletResponse.SC_MOVED_PERMANENTLY;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

import com.google.common.hash.Hashing;
import com.notarius.urlshortener.dto.ShortenUrlRequest;
import com.notarius.urlshortener.dto.FullUrlRequest;
import com.notarius.urlshortener.service.IUrlStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.apache.commons.validator.routines.UrlValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Url services to reduce url.
 *
 * @author cyril.goasduff
 */
@Controller
@RequestMapping("/")
public class UrlController {

    /**
     * Url store services.
     */
    @Autowired
    private IUrlStoreService urlStoreService;

    /**
     * Validation url schemes.
     */
    private final static String[] schemes = {"http","https"};

    /**
     * Url Validator.
     */
    private UrlValidator urlValidator = new UrlValidator(schemes);

    /**
     * Displays the home screen.
     *
     * @param shortRequest Request to have a tiny url
     * @param fullRequest Request to have a full url
     *
     * @return the home screen
     */
    @RequestMapping(value="", method=GET)
    public String showForm(ShortenUrlRequest shortRequest, FullUrlRequest fullRequest) {
        return "shortener";
    }

    /**
     * Displays the second home screen.
     *
     * @param shortRequest Request to have a tiny url
     * @param fullRequest Request to have a full url
     *
     * @return the second home screen
     */
    @RequestMapping(value="full", method=GET)
    public String showFullForm(ShortenUrlRequest shortRequest, FullUrlRequest fullRequest) {
        return "shortener";
    }

    /**
     * Redirects from shortened url to the original one.
     *
     * @param id  Shortened url
     * @param resp Response
     *
     * @throws Exception
     */
    @RequestMapping(value = "{id}", method = GET)
    public void redirectToUrl(@PathVariable String id, HttpServletResponse resp) throws Exception {
        final String url = urlStoreService.findUrlById(id);
        if (url != null) {
            resp.addHeader("Location", url);
            resp.setStatus(SC_MOVED_PERMANENTLY);
        } else {
            resp.sendError(SC_NOT_FOUND);
        }
    }

    /**
     * Get the original url from the shortened one.
     *
     * @param httpRequest HTTP Request
     * @param shortenUrlRequest Request to have a tiny url
     * @param fullUrlRequest Request to have a full url
     * @param bindingResult  Binding Result
     *
     *
     * @return the original url from the shortened one
     */
    @RequestMapping(value="full", method = POST)
    public ModelAndView fullUrl(HttpServletRequest httpRequest,
                                ShortenUrlRequest shortenUrlRequest,
                                @Valid FullUrlRequest fullUrlRequest,
                                BindingResult bindingResult) {
        String id = fullUrlRequest.getUrl();
        final String url = urlStoreService.findUrlById(id);
        if (url == null) {
            bindingResult.addError(new FieldError("fullUrlRequest", "url", id));
        }

        ModelAndView modelAndView = new ModelAndView("shortener");
        if (!bindingResult.hasErrors()) {
            modelAndView.addObject("fullUrl", url);
        }
        shortenUrlRequest.setUrl(null);
        return modelAndView;
    }

    /**
     * Creates a shortened version of the provided url.
     *
     * @param httpRequest HTTP Request
     * @param shortenUrlRequest Request to have a tiny url
     * @param fullUrlRequest Request to have a full url
     * @param bindingResult  Binding Result
     *
     * @return shortened version of the provided url
     */
    @RequestMapping(value="", method = POST)
    public ModelAndView shortenUrl(HttpServletRequest httpRequest,
                                   FullUrlRequest fullUrlRequest,
                                   @Valid ShortenUrlRequest shortenUrlRequest,
                                   BindingResult bindingResult) {
        String url = shortenUrlRequest.getUrl();
        if (!urlValidator.isValid(url)) {
            bindingResult.addError(new FieldError("shortenUrlRequest","url", url));
        }

        ModelAndView modelAndView = new ModelAndView("shortener");
        if (!bindingResult.hasErrors()) {
            final String id = Hashing.murmur3_32()
                .hashString(url, StandardCharsets.UTF_8).toString();
            urlStoreService.storeUrl(id, url);
            String requestUrl = httpRequest.getRequestURL().toString();
            String prefix = requestUrl.substring(0, requestUrl.indexOf(httpRequest.getRequestURI(),
                "http://".length()));

            modelAndView.addObject("shortenedUrl", prefix + "/" + id);
        }
        fullUrlRequest.setUrl(null);
        return modelAndView;
    }
}
