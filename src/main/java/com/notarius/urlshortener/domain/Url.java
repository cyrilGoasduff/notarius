package com.notarius.urlshortener.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

/**
 * URL.
 * 
 * @author cyril.goasduff
 *
 */
@Getter
@Setter
@Document(collection = "url")
public class Url
{

	/**
	 * Tiny URL.
	 */
	@Id
	private String id;

	/**
	 * Original URL.
	 */
	private String original;
}