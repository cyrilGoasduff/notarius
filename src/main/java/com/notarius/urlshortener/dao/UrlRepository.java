package com.notarius.urlshortener.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.notarius.urlshortener.domain.Url;

/**
 * Repository {@link Url}.
 *
 * @author cyril.goasduff
 */
public interface UrlRepository
        extends MongoRepository<Url, String>
{
    public Url findById(String id);
}
