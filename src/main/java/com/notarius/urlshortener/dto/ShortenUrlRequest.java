package com.notarius.urlshortener.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Shorten url request.
 *
 * @author cyril.goasduff
 */
@Getter
@Setter
@NoArgsConstructor
public class ShortenUrlRequest {

    /**
     * Url to reduce.
     */
    @NotNull
    @Size(min = 7)
    private String url;
}
