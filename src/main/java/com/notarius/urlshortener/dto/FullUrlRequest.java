package com.notarius.urlshortener.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Full url request.
 *
 * @author cyril.goasduff
 */
@Getter
@Setter
@NoArgsConstructor
public class FullUrlRequest {

    /**
     * The reduced Url.
     */
    @NotNull
    @Size(max = 10)
    private String url;
}
