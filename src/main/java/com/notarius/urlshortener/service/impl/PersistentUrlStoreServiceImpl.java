package com.notarius.urlshortener.service.impl;

import com.notarius.urlshortener.domain.Url;
import com.notarius.urlshortener.service.IUrlStoreService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.notarius.urlshortener.dao.UrlRepository;

/**
 * Implementation of the interface of the url store services.
 *
 * @author cyril.goasduff
 */
@Service
public class PersistentUrlStoreServiceImpl implements IUrlStoreService {

    /**
     * Repository des URL.
     */
    @Autowired
    private UrlRepository urlRepository;

    /*
     * (non-Javadoc)
     * @see com.notarius.urlshortener.service.IUrlStoreService#findUrlById(java.lang.String)
     */
    @Override
    public String findUrlById(String id) {

        String ret = null;
        Url db = urlRepository.findById(id);
        if(db != null){
            ret = db.getOriginal();
        }
        return ret;
    }

    /*
     * (non-Javadoc)
     * @see com.notarius.urlshortener.service.IUrlStoreService#storeUrl(java.lang.String, java.lang.String)
     */
    @Override
    public void storeUrl(String id, String url) {
        Url db = new Url();
        db.setId(id);
        db.setOriginal(url);

        urlRepository.save(db);
    }
}
