package com.notarius.urlshortener.service;

/**
 * Interface of the url store services.
 *
 * @author cyril.goasduff
 */
public interface IUrlStoreService {

    /**
     * Get a full url by a tiny url.
     *
     * @param id The tiny url
     *
     * @return The full url correspnding to the tiny url
     */
    String findUrlById(String id);

    /**
     * Save the
     * @param id
     * @param url
     */
    void storeUrl(String id, String url);
}
