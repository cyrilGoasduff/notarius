# urlShortener
Url-shortener web application base on this project here: http://kaviddiss.com/2015/07/18/building-modern-web-applications-using-java-spring/.

To run it using maven (http://maven.apache.org):
```sh
$ mvn spring-boot:run
```

Then navigate to http://localhost:8080/ in your browser.

For english version, navigate to http://localhost:8080/?lang=en
